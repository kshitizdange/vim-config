set showcmd             " display incomplete commands
set number              " row numbers
set incsearch           " incremental search
set ignorecase          " ignores case while searching
set smartcase           " case sensitive only when using capital letters
set cursorline          " highlights the cursor line
set completeopt=menuone    " show completion menu also for single hits

let c_space_errors = 1
let java_space_errors = 1
let c_no_trail_space_error = 1
let c_no_tab_space_error = 1
set shiftwidth =4
set expandtab
set softtabstop =4
set autoindent
set smartindent
 
" Persistent undo - new function in Vim 7.3.
" Create the directory manually.
set undodir=~/.vim/undodir
set undofile
 
syntax on
colorscheme molokai

set tags=./tags,tags;$HOME
